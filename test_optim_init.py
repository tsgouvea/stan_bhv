import numpy as np
import pystan
import seaborn as sns
import matplotlib.pyplot as plt

from locallib import StanModel_cache

#%%

N = 1000
stim = np.random.rand(N)
perc = stim + np.random.rand(N)*.5-.25
L = 2
state = np.random.randint(1,3,N).astype(int)
cho = perc > .4
cho[state==2] = perc[state==2] > .6

# hf, ha = plt.subplots(1,1)
# sns.regplot(stim,cho,logistic=True,ax=ha,ci=None)

stan_data = {'N':N,
             'L':L,
             'state':state,
             'stim':stim,
             'cho':cho.astype(int)}

stan_code = """
data {
  int N;
  int L;
  vector[N] stim;
  int cho[N];
  int state[N];
}
parameters {
  real<lower=0,upper=1> bias[L];
  real<lower=-100,upper=100> slope;
}
transformed parameters{
  vector[N] DV;
  for (n in 1:N){
    DV[n] = (stim[n]-bias[state[n]])*slope;
  }
}
model{
  cho ~ bernoulli_logit(DV);
}
"""
sm = StanModel_cache(model_code=stan_code)

#%%
fit = sm.sampling(data=stan_data, warmup=1000, iter=2000, chains=3, verbose=True)

#%%
flatnames = np.array(fit.flatnames)
flatnames = list(flatnames[[not n.startswith('D') for n in flatnames]])

temp=fit.to_dataframe(pars=flatnames)
# init=[temp.loc[temp.chain==c,:].mean().to_dict() for c in temp.chain.drop_duplicates().values]

init=[temp.loc[temp.chain==c,:].mean().to_dict() for c in temp.chain.drop_duplicates().values for temp in [fit.to_dataframe(pars=flatnames)]]
for i in range(len(init)):init[i]['bias'] = [init[i]['bias[1]'],init[i]['bias[2]']]
# sm.optimizing(data=stan_data)
#
# sm.optimizing(data=stan_data,init=init)

#%%
N = 1000
L = 4
S = 20

n = np.arange(N)+1
l = np.random.choice(L,N)
l.sort()
s = np.random.choice(S,N)
s.sort()

x1 = np.random.randn(N)*10
x2 = x1>0
x2 = x2.astype(float)