import os
import pickle

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from locallib import fit, optim

for imodel in np.sort(np.hstack((np.arange(2, 13),np.array([102,107,108,110,111,112,207,211,212])))):
    model_name = 'm{:03d}'.format(imodel)
    print(model_name)
    for dataset in ['matching_conf','matching_fix','matching_blockLen10','matching_blockLen100',
                    'matching_blockLen100e','matching_blockLen1000','matching_cdda2c5']:
        print(dataset)
        try:
            fit(dataset,model_name,iter=400)
        except Exception as e:
            print(e)
        try:
            optim(dataset,model_name,init='fit',iter=4000)
        except Exception as e:
            print(e)