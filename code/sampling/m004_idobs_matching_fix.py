#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

#%%
sm_m004 = pickle.load(open(os.path.join('models', np.array(os.listdir("models"))[['m004' in n for n in os.listdir("models")]].item()), 'rb'))
stan_data = pickle.load(open(os.path.join('data','stan_matching_fix.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
dict_sess = stan_data.pop('dict_sess')

#%%
fname_fit_m004 = os.path.join('data','fit','m004_matching_fix.pickle')
if os.path.isfile(fname_fit_m004):
    with open(fname_fit_m004,'rb') as fhandle:
        d = pickle.load(fhandle)
        fit_m004 = d['fit']
        del d
else:
    fit_m004 = sm_m004.sampling(data=stan_data, warmup = 1000, iter=1500, chains=3,verbose=True,
                                sample_file=os.path.join('data','fit','m004_matching_fix_samples.csv'))
    pickle.dump({'model': sm_m004, 'fit': fit_m004}, open(fname_fit_m004, 'wb'), protocol=-1)

#%%
myvars = np.array(fit_m004.flatnames)[[not n.startswith('log_like') and not n.startswith('Q') and n.startswith('eta') for n in fit_m004.flatnames]]
df_diag_m004 = fit_m004.to_dataframe(pars=list(myvars), permuted=True)
path_figures = os.path.join(os.getcwd(),'figures','m004_matching_fix')
os.makedirs(path_figures, exist_ok=True)
# for isubj,subj in dict_subj.items():
#     myvars = np.array(fit_m004.flatnames)[[not n.startswith('log_like') and not n.startswith('Q') and n.endswith('{}]'.format(isubj)) and not n.startswith('eta') for n in fit_m004.flatnames]]
hf = sns.pairplot(df_diag_m004, hue='chain', vars=myvars, diag_kind='kde')
plt.tight_layout()
hf.savefig(os.path.join(path_figures,"pairplot_m004_matching_fix.png"))
print(df_diag_m004.loc[:, myvars].mean())