#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

#%%
sm_m009 = pickle.load(open(os.path.join('models', np.array(os.listdir("models"))[['m009' in n for n in os.listdir("models")]].item()), 'rb'))
stan_data = pickle.load(open(os.path.join('data','stan_matching_fix.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
dict_sess = stan_data.pop('dict_sess')

#%%
fname_fit_m009 = os.path.join('data','fit','m009_matching_fix.pickle')
if os.path.isfile(fname_fit_m009):
    with open(fname_fit_m009,'rb') as fhandle:
        d = pickle.load(fhandle)
        fit_m009 = d['fit']
        del d
else:
    fit_m009 = sm_m009.sampling(data=stan_data, warmup = 1000, iter=2000, chains=3,verbose=True,
                                sample_file=os.path.join('data','fit','m009_matching_fix_samples.csv'))
    pickle.dump({'model': sm_m009, 'fit': fit_m009}, open(fname_fit_m009, 'wb'), protocol=-1)

#%%

myvars = np.array(fit_m009.flatnames)[[not n.startswith('log_like') and not n.startswith('Q') for n in fit_m009.flatnames]]
df_diag_m009 = fit_m009.to_dataframe(pars=list(myvars), permuted=True)
path_figures = os.path.join(os.getcwd(),'figures','m009_matching_fix')
os.makedirs(path_figures, exist_ok=True)
for isubj,subj in dict_subj.items():
    myvars2 = myvars[[n.endswith('{}]'.format(isubj)) for n in myvars]]
    hf = sns.pairplot(df_diag_m009, hue='chain', vars=myvars2, diag_kind='kde')
    plt.suptitle(subj)
    plt.tight_layout()
    hf.savefig(os.path.join(path_figures,"pairplot_m009_matching_fix_{}.png".format(subj)))
    print(df_diag_m009.loc[:, myvars2].mean())