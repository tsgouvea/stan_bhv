#!/usr/bin/env python
# coding: utf-8

# Model 001:
# one (eta, beta) pair per session.
# learn their distributions, use as priors

import os
import pickle

import numpy as np
import seaborn as sns

from locallib import StanModel_cache

# decision completely controlled by learn_pi estimate of reward probability
# ## stan_code_idobs
# ### without Beta, hierarchical Eta

stan_code_m001 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  real<lower=0,upper=50> m1;
  real<lower=0,upper=50> s1;
  real<lower=0,upper=50> p1;
  real<lower=0,upper=50> m2;
  real<lower=0,upper=50> s2;
  real<lower=0,upper=50> p2;
  real<lower=0,upper=50> m3;
  real<lower=0,upper=50> s3;
  real<lower=0,upper=50> p3;
  real<lower=0,upper=50> m4;
  real<lower=0,upper=50> s4;
  real<lower=0,upper=50> p4;
  vector<lower=0,upper=50>[S] beta;
  vector<lower=0,upper=1>[S] eta;
}
transformed parameters {
  vector[N] Ql_e;
  vector[N] Qr_e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_e[n] = 1-(1+exp(piL))^(-kL);
    Qr_e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      temp = kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      piL = piL - eta[subj[n]]*temp;
      kL = 1;
      kR += 1;
    }
    else {
      temp = kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      piR = piR  - eta[subj[n]]*temp;
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  m1 ~ cauchy(0,50);
  s1 ~ cauchy(0,50);
  p1 ~ gamma(m1,s1);
  m2 ~ cauchy(0,50);
  s2 ~ cauchy(0,50);
  p2 ~ gamma(m2,s2);
  m3 ~ cauchy(0,50);
  s3 ~ cauchy(0,50);
  p3 ~ gamma(m3,s3);
  m4 ~ cauchy(0,10);
  s4 ~ cauchy(0,50);
  p4 ~ gamma(m4,s4);
  
  for (s in 1:S){
    beta[s] ~ gamma(p1,p2);
    eta[s] ~ beta(p3,p4);    
  }
  isChoiceLeft ~ bernoulli_logit(beta[subj].*(Ql_e-Qr_e));
}
"""

"""generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | beta[subj[n]].*(Ql_e[n]-Qr_e[n]));
  }
}"""

sm_m001 = StanModel_cache(model_code=stan_code_m001)

with open(os.path.join('data', 'stan_data.pickle'), 'rb') as fhandle:
    stan_data = pickle.load(fhandle)

fname_fit_m001 = os.path.join('data','fit_m001.pickle')
if os.path.isfile(fname_fit_m001):
    with open(fname_fit_m001,'rb') as fhandle:
        d = pickle.load(fhandle)
        fit_m001 = d['fit']
else:
    fit_m001 = sm_m001.sampling(data=stan_data, iter=1000, chains=4,verbose=True)
    pickle.dump({'model': sm_m001, 'fit': fit_m001}, open(fname_fit_m001, 'wb'), protocol=-1)

#print(fit_m001)

myvars = np.array(fit_m001.flatnames)[[not n.startswith('log_like') and not n.startswith('Q') for n in fit_m001.flatnames]]
df_diag_m001 = fit_m001.to_dataframe()

#hf = sns.pairplot(df_diag_m001, hue='chain', vars=myvars,kind='kde', diag_kind='kde')
hf = sns.pairplot(df_diag_m001, hue='chain', vars=['p1','p2','p3','p4','beta','eta'],kind='kde', diag_kind='kde')

hf.savefig("notebooks/pairplot_idobs_m001.png")

print(df_diag_m001.loc[:,['p1','p2','p3','p4','beta','eta']].mean())

print(df_diag_m001.loc[:,['s1','m1','p1','s2','m2','p2','s3','m3','p3','s4','m4','p4']].mean())