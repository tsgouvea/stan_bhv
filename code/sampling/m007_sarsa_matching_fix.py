#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

#%%
sm_m007 = pickle.load(open(os.path.join('models', np.array(os.listdir("models"))[['m007' in n for n in os.listdir("models")]].item()), 'rb'))
stan_data = pickle.load(open(os.path.join('data','stan_matching_fix.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
dict_sess = stan_data.pop('dict_sess')

#%%
fname_fit_m007 = os.path.join('data','fit','m007_matching_fix.pickle')
if os.path.isfile(fname_fit_m007):
    with open(fname_fit_m007,'rb') as fhandle:
        d = pickle.load(fhandle)
        fit_m007 = d['fit']
        del d
else:
    fit_m007 = sm_m007.sampling(data=stan_data, warmup = 1000, iter=2000, chains=3,verbose=True,
                                sample_file=os.path.join('data','fit','m007_matching_fix_samples.csv'))
    pickle.dump({'model': sm_m007, 'fit': fit_m007}, open(fname_fit_m007, 'wb'), protocol=-1)

#%%

myvars = np.array(fit_m007.flatnames)[[not n.startswith('log_like') and not n.startswith('Q') and not n.startswith('alpha') and not n.startswith('phi') and not n.startswith('beta') for n in fit_m007.flatnames]]
df_diag_m007 = fit_m007.to_dataframe(pars=list(myvars), permuted=True)
path_figures = os.path.join(os.getcwd(),'figures','m007_matching_fix')
os.makedirs(path_figures, exist_ok=True)
for isubj,subj in dict_subj.items():
    myvars2 = myvars[[n.endswith('{}]'.format(isubj)) for n in myvars]]
    hf = sns.pairplot(df_diag_m007, hue='chain', vars=myvars2, diag_kind='kde')
    plt.suptitle(subj)
    plt.tight_layout()
    hf.savefig(os.path.join(path_figures,"pairplot_m007_matching_fix_{}.png".format(subj)))
    print(df_diag_m007.loc[:, myvars2].mean())