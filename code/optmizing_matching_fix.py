#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

#%%
stan_data = pickle.load(open(os.path.join('data','stan_matching_fix.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
dict_sess = stan_data.pop('dict_sess')

for d in range(2,10):
    try:
        model_name = 'm{:03d}'.format(d)
        print(model_name)
        sm = pickle.load(open(os.path.join('models',
                                           np.array(os.listdir("models"))[[model_name in n for n in os.listdir("models")]].item()), 'rb'))
        fname_optim = os.path.join('data', 'optim', model_name + '_matching_fix.pickle')
        if not os.path.isfile(fname_optim):
            try:
                optim = sm.optimizing(data=stan_data, verbose=True, save_iterations=True,
                                      sample_file=os.path.join('data', 'optim', model_name + '_matching_fix_samples.csv'))
                pickle.dump({'model': sm, 'optim': optim}, open(fname_optim, 'wb'), protocol=-1)
            except Exception as e:
                print(e)
                print("Trying Newton method")
                optim = sm.optimizing(data=stan_data, verbose=True, save_iterations=True, algorithm='Newton',
                                      sample_file=os.path.join('data', 'optim',
                                                               model_name + '_matching_fix_samples.csv'))
                pickle.dump({'model': sm, 'optim': optim}, open(fname_optim, 'wb'), protocol=-1)
    except Exception as e:
        print(e)