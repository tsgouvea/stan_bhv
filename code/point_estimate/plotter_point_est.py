import os
import pickle

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

# %%

set_vars_S = dict()
set_vars_L = dict()

set_vars_S[2] = ['eta']
set_vars_L[2] = ['a_subj', 'b_subj']

set_vars_S[4] = []
set_vars_L[4] = ['eta']

set_vars_S[8] = ['eta','beta']
set_vars_L[8] = ['a_subj', 'b_subj', 'c_beta']

set_vars_S[10] = ['eta','pizero']
set_vars_L[10] = ['a_eta', 'b_eta', 'mu_pizero', 'sigma_pizero']

set_vars_S[3] = []
set_vars_L[3] = ['alpha', 'phi', 'beta']

set_vars_S[5] = []
set_vars_L[5] = ['alpha', 'phi']

set_vars_S[6] = []
set_vars_L[6] = ['alpha', 'beta']

set_vars_S[7] = ['alpha','phi','beta']
set_vars_L[7] = ['a_alpha','b_alpha', 'a_phi', 'b_phi', 'c_beta']

set_vars_S[9] = []
set_vars_L[9] = ['alpha']

set_vars_S[102] = ['eta']
set_vars_L[102] = ['a_subj', 'b_subj']

set_vars_S[108] = ['eta','beta']
set_vars_L[108] = ['a_subj', 'b_subj', 'c_beta']

set_vars_S[110] = ['eta','pizero']
set_vars_L[110] = ['a_eta', 'b_eta', 'mu_pizero', 'sigma_pizero']

set_vars_S[107] = ['alpha','phi','beta']
set_vars_L[107] = ['a_alpha','b_alpha', 'a_phi', 'b_phi', 'c_beta']

df_params = pd.concat((pd.DataFrame(index=set_vars_L.keys(),columns=['params_L'],data=[[n] for n in set_vars_L.values()]),
                       pd.DataFrame(index=set_vars_S.keys(),columns=['params_S'],data=[[n] for n in set_vars_S.values()])),
                      sort=False,axis=1)

# df_params.to_html('models/model_params.html')
# df_params.to_latex('models/model_params.tex')

#%%
for dataset in ['matching_fix', 'matching_conf']:
    # if dataset == 'matching_fix': continue
    print(dataset)
    stan_data = pickle.load(open(os.path.join('data', 'stan_{}.pickle'.format(dataset)), 'rb'))
    dict_subj = stan_data.pop('dict_subj')
    dict_sess = stan_data.pop('dict_sess')

    path_fig_ds = os.path.join('figures', 'point_est')
    os.makedirs(path_fig_ds,exist_ok=True)
#%%
    for imodel in [102,107,108,110]:# range(10, 11):
        model_name = 'm{:03d}'.format(imodel)
        print(model_name)
        try:
            # fit(dataset,model_name)
            print('\tLoading vars')

            d = pickle.load(open(os.path.join('data', 'optim', 'optim_{}_{}.pickle'.format(model_name, dataset)), 'rb'))
            optim_idobs = d['optim']
            # del d

            d = pickle.load(open(os.path.join('data', 'fit', 'fit_{}_{}.pickle'.format(model_name, dataset)), 'rb'))
            fit_idobs = d['fit']
            # del d
            # %%
            # unique_vars = np.unique([n.split('[')[0] for n in fit_idobs.flatnames])
            # print(unique_vars)
            flatnames = np.array(fit_idobs.flatnames)
            vars_S = np.array(set_vars_S[imodel])
            vars_L = np.array(set_vars_L[imodel])
            unique_vars = np.hstack((vars_S, vars_L))
            flatnames = flatnames[[np.any([f.startswith(v) for v in unique_vars]) for f in flatnames]]

            # %%
            print('\tComputing diagnostics')
            df_diag = fit_idobs.to_dataframe(pars=list(flatnames), permuted=True)

            # %%
            print('\tGenerating figures')
            for isubj, subj in dict_subj.items():
                hf_name = os.path.join(path_fig_ds,"{}_{}_{}_pairplot.png".format(subj, model_name, dataset))
                hf2_name = os.path.join(path_fig_ds,"{}_{}_{}_marginals.png".format(subj, model_name, dataset))
                if os.path.isfile((hf_name)) and os.path.isfile((hf2_name)):
                    continue
                try:
                    print(isubj, subj)
                    sess = np.unique(np.array(stan_data['sess'])[np.array(stan_data['subj']) == isubj])
                    subj_vars_S = [var + '[{}]'.format(isess) for isess in sess for var in vars_S]
                    subj_vars_L = [var + '[{}]'.format(isubj) for var in vars_L]
                    subj_vars = subj_vars_L + subj_vars_S
                    hf = sns.pairplot(df_diag, hue='chain', x_vars=subj_vars, y_vars=subj_vars_L, diag_kind='kde')
                    plt.suptitle(subj)
                    plt.tight_layout()
                    hf.savefig(hf_name)
                    plt.close(hf.fig)
                    del hf
                    # print(df_diag.loc[:, subj_vars].mean())

                    nvars = len(subj_vars)
                    ncol = max(2,int(np.ceil(np.sqrt(nvars))))
                    nrow = max(2,int(np.ceil(nvars / ncol)))
                    hf2, ha2 = plt.subplots(nrow, ncol, figsize=(10, 10))
                    irow = 0
                    icol = 0
                    for ivar, var in enumerate(subj_vars):
                        for chain in df_diag.chain.drop_duplicates():
                            ndx = df_diag.chain == chain
                            sns.kdeplot(df_diag.loc[ndx, var], ax=ha2[irow, icol], legend=False)
                        ha2[irow, icol].set_title(var)
                        opt_pe = optim_idobs[var.split('[')[0]][int(var.split('[')[1].split(']')[0]) - 1]
                        ha2[irow, icol].scatter(opt_pe, 0, color='r')
                        irow = irow + 1 if icol == ncol - 1 else irow
                        icol = 0 if icol == ncol - 1 else icol + 1

                    plt.tight_layout()
                    hf2.savefig(hf2_name)
                    plt.close(hf2)
                    del hf2
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)