#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import arviz as az

#%%
def getaz(fname_az):
    if os.path.isfile(fname_az):
        print('\tLoading arviz InferenceData file from disk')
        dfaz = pickle.load(open(fname_az, 'rb'))
    else:
        print('\tMaking arviz InferenceData file.\nLoading fit file from disk')
        d = pickle.load(open(os.path.join('data', 'fit', 'fit_{}_{}.pickle'.format(model_name, dataset)), 'rb'))
        fit = d['fit']
        del d
        dfaz = az.from_pystan(fit, log_likelihood='log_likelihood')
        pickle.dump(dfaz, open(fname_az, 'wb'), protocol=-1)
    return dfaz

#%%
for dataset in ['matching_fix', 'matching_conf']:
    print(dataset)
    stan_data = pickle.load(open(os.path.join('data', 'stan_{}.pickle'.format(dataset)), 'rb'))
    dict_subj = stan_data.pop('dict_subj')
    dict_sess = stan_data.pop('dict_sess')

    path_fig_ds = os.path.join('figures', 'point_est', dataset)
    os.makedirs(path_fig_ds,exist_ok=True)
    path_az_ds = os.path.join('data', 'arviz', dataset)
    os.makedirs(path_az_ds, exist_ok=True)
    fname_df_compare = os.path.join('data','model_compare_{}.pickle'.format(dataset))

    dataset_dict = {}

    for imodel in [102,107,108,110,111,112]: #%[]:
        model_name = 'm{:03d}'.format(imodel)
        print('\t',model_name)
        try:
            fname_az = os.path.join(path_az_ds,'az_{}_{}.pickle'.format(model_name, dataset))
            dfaz = getaz(fname_az)
            dataset_dict[model_name] = dfaz
        except Exception as e:
            print(e)
        # break
#%%

    df_compare = az.compare(dataset_dict)
    print(df_compare)
    pickle.dump(df_compare,open('data/model_compare_{}.pickle'.format(dataset),'wb'),protocol=-1)

