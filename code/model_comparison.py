#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
import arviz as az

#%%
for dataset in ['matching_fix', 'matching_conf']:
    print('\n' + dataset)
    stan_data = pickle.load(open(os.path.join('data', 'stan_{}.pickle'.format(dataset)), 'rb'))
    dict_subj = stan_data.pop('dict_subj')
    dict_sess = stan_data.pop('dict_sess')

    path_fig_ds = os.path.join('figures', 'point_est', dataset)
    os.makedirs(path_fig_ds,exist_ok=True)
    path_az_ds = os.path.join('data', 'arviz', dataset)
    os.makedirs(path_az_ds, exist_ok=True)

    dataset_dict = {}

    fname_modcomp = 'data/model_compare_{}.pickle'.format(dataset)
    if os.path.isfile(fname_modcomp):
        print('loading {}'.format(fname_modcomp))
        old_df_compare = pickle.load(open(fname_modcomp, 'rb'))

    for imodel in np.sort(np.hstack((np.arange(2, 13),np.array([102,107,108,110,111,112])))):
        model_name = 'm{:03d}'.format(imodel)
        print(model_name)
        if 'old_df_compare' in locals() and model_name in old_df_compare.index:
            print('Skipping model {}: already in comparison table.'.format(model_name))
            continue
        try:
            fname_az = os.path.join(path_az_ds,'az_{}_{}.pickle'.format(model_name, dataset))
            if os.path.isfile(fname_az):
                print('Loading arviz InferenceData file from disk')
                dfaz = pickle.load(open(fname_az, 'rb'))
            else:
                try:
                    print('Making arviz InferenceData file.\nLoading fit file from disk')
                    d = pickle.load(open(os.path.join('data', 'fit', 'fit_{}_{}.pickle'.format(model_name, dataset)), 'rb'))
                    fit = d['fit']
                    del d
                    dfaz = az.from_pystan(fit, log_likelihood='log_likelihood')
                    pickle.dump(dfaz, open(fname_az, 'wb'), protocol=-1)
                    # del dfaz
                except Exception as e:
                    print(e)
            # dataset_dict[model_name] = dfaz
        except Exception as e:
            print(e)
        else:
            temp = az.compare({model_name:dfaz})
            df_compare = pd.concat((old_df_compare,temp)) if 'old_df_compare' in locals() else temp
            df_compare.drop_duplicates(inplace=True)
            df_compare.sort_values('waic',inplace=True)
            old_df_compare = df_compare
            pickle.dump(df_compare, open(fname_modcomp, 'wb'), protocol=-1)
            df_compare.to_html('data/model_compare_{}.html'.format(dataset))
        if 'dfaz' in locals(): del dfaz
    if 'old_df_compare' in locals(): del old_df_compare
    print('done with {}'.format(dataset))

    # print(df_compare)
    # fname_modcomp = 'data/model_compare_{}.pickle'.format(dataset)
    # if of.path.isfile(fname_modcomp):
    #     old_df_compare = pickle.load(open(fname_modcomp,'rb'))
    #     df_compare = pd.concat((old_df_compare,df_compare))
    #     df_compare.drop_duplicates(inplace=True)
    # pickle.dump(df_compare,open(fname_modcomp,'wb'),protocol=-1)
    # df_compare.to_html('data/model_compare_{}.html'.format(dataset))