import os
import pickle

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

stan_data = pickle.load(open(os.path.join('data','stan_matching_conf.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
dict_sess = stan_data.pop('dict_sess')

#%% IDOBS

model_name = 'm{:03d}'.format(10)
print('Loading vars IDOBS')

d = pickle.load(open(os.path.join('data', 'optim', model_name + '_matching_conf.pickle'), 'rb'))
optim_idobs = d['optim']
del d

#%% SARSA

model_name = 'm{:03d}'.format(3)
print('Loading vars SARSA')

d = pickle.load(open(os.path.join('data', 'optim', model_name + '_matching_conf.pickle'), 'rb'))
optim_sarsa = d['optim']
del d


#%%
df_data = pd.DataFrame({n: np.array(stan_data[n]) for n in ['isChoiceLeft', 'isRewarded', 'subj', 'sess', 'waitingTime', 'reactionTime', 'movementTime']})
df_data.loc[:,'Ql_idobs'] = optim_idobs['Ql_e']
df_data.loc[:,'Qr_idobs'] = optim_idobs['Qr_e']
df_data.loc[:,'greedy_idobs'] = ((df_data.loc[:,'Ql_idobs'] > df_data.loc[:,'Qr_idobs']) == df_data.loc[:,'isChoiceLeft'])
df_data.loc[:,'dv_idobs'] = np.log(1e-10+df_data.loc[:,'Ql_idobs']) - np.log(1e-10+df_data.loc[:,'Qr_idobs'])

df_data.loc[:,'Ql_sarsa'] = optim_sarsa['Ql_sarsa']
df_data.loc[:,'Qr_sarsa'] = optim_sarsa['Qr_sarsa']
df_data.loc[:,'greedy_sarsa'] = ((df_data.loc[:,'Ql_sarsa'] > df_data.loc[:,'Qr_sarsa']) == df_data.loc[:,'isChoiceLeft'])
df_data.loc[:,'dv_sarsa'] = np.log(1e-10+df_data.loc[:,'Ql_sarsa']) - np.log(1e-10+df_data.loc[:,'Qr_sarsa'])

#%%
nbins = 20
fit_reg = False
y_jitter = 0.1
for isubj,subj in dict_subj.items():
    try:
        df_data_subj = df_data.loc[df_data.subj == isubj,:].copy()
        hf, ha = plt.subplots(2,2,sharey='row')

        #%% Choice
        sns.regplot(x='dv_sarsa',y='isChoiceLeft',ax=ha[0,0],data=df_data_subj,
                    color='xkcd:black', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_idobs',y='isChoiceLeft',ax=ha[0,1],data=df_data_subj,
                    color='xkcd:black', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        #%% WT
        sns.regplot(x='dv_sarsa',y='reactionTime',ax=ha[1,0],data=df_data_subj.loc[df_data_subj.greedy_sarsa,:],
                    color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_sarsa',y='reactionTime',ax=ha[1,0],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_sarsa),:],
                    color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_idobs',y='reactionTime',ax=ha[1,1],data=df_data_subj.loc[df_data_subj.greedy_idobs,:],
                    color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_idobs',y='reactionTime',ax=ha[1,1],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_idobs),:],
                    color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        plt.tight_layout()
        plt.suptitle(subj)
        hf.savefig(os.path.join('figures', 'times', "{}_{}_RTs.png".format(subj,'matching_conf')))
    except Exception as e:
        print(e)

#%%
print("plotting waiting times")
for isubj,subj in dict_subj.items():
    try:
        df_data_subj = df_data.loc[df_data.subj == isubj,:].copy()
        hf, ha = plt.subplots(2,2,sharey='row')

        #%% Choice
        sns.regplot(x='dv_sarsa',y='isChoiceLeft',ax=ha[0,0],data=df_data_subj,
                    color='xkcd:black', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_idobs',y='isChoiceLeft',ax=ha[0,1],data=df_data_subj,
                    color='xkcd:black', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        #%% WT
        sns.regplot(x='dv_sarsa',y='waitingTime',ax=ha[1,0],data=df_data_subj.loc[df_data_subj.greedy_sarsa,:],
                    color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_sarsa',y='waitingTime',ax=ha[1,0],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_sarsa),:],
                    color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_idobs',y='waitingTime',ax=ha[1,1],data=df_data_subj.loc[df_data_subj.greedy_idobs,:],
                    color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        sns.regplot(x='dv_idobs',y='waitingTime',ax=ha[1,1],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_idobs),:],
                    color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

        plt.tight_layout()
        plt.suptitle(subj)
        hf.savefig(os.path.join('figures', 'times', "{}_{}_WTs.png".format(subj,'matching_conf')))
    except Exception as e:
        print(e)