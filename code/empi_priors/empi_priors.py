import os
import pickle

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

# %%

set_vars_S = dict()
set_vars_L = dict()

set_vars_S[2] = ['eta']
set_vars_L[2] = ['a_subj', 'b_subj']

set_vars_S[4] = []
set_vars_L[4] = ['eta']

set_vars_S[8] = ['eta','beta']
set_vars_L[8] = ['a_subj', 'b_subj', 'c_beta']

set_vars_S[10] = ['eta','pizero']
set_vars_L[10] = ['a_eta', 'b_eta', 'mu_pizero', 'sigma_pizero']

set_vars_S[11] = ['eta','pizero', 'beta']
set_vars_L[11] = ['a_eta', 'b_eta', 'mu_pizero', 'sigma_pizero', 'c_beta']

set_vars_S[3] = []
set_vars_L[3] = ['alpha', 'phi', 'beta']

set_vars_S[5] = []
set_vars_L[5] = ['alpha', 'phi']

set_vars_S[6] = []
set_vars_L[6] = ['alpha', 'beta']

set_vars_S[7] = ['alpha','phi','beta']
set_vars_L[7] = ['a_alpha','b_alpha', 'a_phi', 'b_phi', 'c_beta']

set_vars_S[9] = []
set_vars_L[9] = ['alpha']

set_vars_S[12] = ['eta','pizero', 'betai', 'alpha', 'phi', 'betas', 'w']
set_vars_L[12] = ['a_eta', 'b_eta', 'mu_pizero', 'sigma_pizero', 'c_betai', 'a_alpha','b_alpha', 'a_phi', 'b_phi', 'c_betas',
                  'a_w', 'b_w']

df_params = pd.concat((pd.DataFrame(index=set_vars_L.keys(),columns=['params_L'],data=[[n] for n in set_vars_L.values()]),
                       pd.DataFrame(index=set_vars_S.keys(),columns=['params_S'],data=[[n] for n in set_vars_S.values()])),
                      sort=False,axis=1)

#%%
path_fig_ds = os.path.join('figures', 'empi_priors')
os.makedirs(path_fig_ds,exist_ok=True)

for imodel in [12]:# [11, 10, 7, 2, 8]:
    model_name = 'm{:03d}'.format(imodel)
    print(model_name)
    for dataset in ['matching_fix', 'matching_conf']:
        print(dataset)
        stan_data = pickle.load(open(os.path.join('data', 'stan_{}.pickle'.format(dataset)), 'rb'))
        dict_subj = stan_data.pop('dict_subj')
        dict_sess = stan_data.pop('dict_sess')
        print('\tLoading vars')

        d = pickle.load(open(os.path.join('data', 'fit', 'fit_{}_{}.pickle'.format(model_name, dataset)), 'rb'))
        fit_idobs = d['fit']
        # del d
        flatnames = np.array(fit_idobs.flatnames)
        vars_S = np.array(set_vars_S[imodel])
        vars_L = np.array(set_vars_L[imodel])
        unique_vars = np.hstack((vars_S, vars_L))
        flatnames = flatnames[[np.any([f.startswith(v) for v in unique_vars]) for f in flatnames]]

        # %%
        print('\tComputing diagnostics')
        df_diag = fit_idobs.to_dataframe(pars=list(flatnames), permuted=True)

        #%%
        df2 = pd.DataFrame() if 'df2' not in locals() else df2
        for isubj, subj in dict_subj.items():
            temp = ['{}[{}]'.format(var,isubj) for var in vars_L]
            temp.append('chain')
            temp2 = df_diag.loc[:,temp].copy()
            temp2.columns = np.hstack((vars_L,'chain'))
            temp2.loc[:, 'subj']=subj
            temp2.loc[:, 'dataset'] = dataset
            for suffix in ['eta','alpha','phi','subj']:
                if 'b_{}'.format(suffix) in vars_L:
                    temp2.loc[:, 'ba'] = np.log(np.divide(temp2.loc[:, 'b_{}'.format(suffix)],temp2.loc[:, 'a_{}'.format(suffix)]))
                    # temp2.loc[:, 'ba'] = np.clip(np.divide(temp2.loc[:, 'b_{}'.format(suffix)],temp2.loc[:, 'a_{}'.format(suffix)]),
                    #                              0,250)
                    if 'ba' not in vars_L: vars_L = np.hstack(('ba', vars_L))
            # if 'b_eta' in vars_L:
            #     temp2.loc[:, 'ba'] = np.divide(temp2.loc[:, 'b_eta'],temp2.loc[:, 'a_eta'])
            #     if 'ba' not in vars_L: vars_L = np.hstack(('ba', vars_L))
            # if 'b_alpha' in vars_L:
            #     temp2.loc[:, 'ba'] = np.divide(temp2.loc[:, 'b_alpha'],temp2.loc[:, 'a_alpha'])
            #     if 'ba' not in vars_L: vars_L = npolumns.hstack(('ba', vars_L))
            # if 'b_phi' in vars_L:
            #     temp2.loc[:, 'ba'] = np.divide(temp2.loc[:, 'b_phi'],temp2.loc[:, 'a_phi'])
            #     if 'ba' not in vars_L: vars_L = np.hstack(('ba', vars_L))
            # if 'b_subj' in vars_L:
            #     temp2.loc[:, 'ba'] = np.divide(temp2.loc[:, 'b_subj'],temp2.loc[:, 'a_subj'])
            #     if 'ba' not in vars_L: vars_L = np.hstack(('ba', vars_L))
            df2 = pd.concat((df2,temp2),sort=False)

        # %%
    print('\tGenerating figures')
    hf_name = os.path.join(path_fig_ds,"empi_priors_{}_per_subj.png".format(model_name))
    if not os.path.isfile(hf_name):
        hf = sns.pairplot(df2, vars=vars_L, hue='subj', diag_kind='kde',plot_kws={'alpha':0.05})
        plt.suptitle(model_name)
        plt.tight_layout()
        hf.savefig(hf_name)
        plt.close(hf.fig)
        del hf

    hf_name = os.path.join(path_fig_ds, "empi_priors_{}_per_dataset.png".format(model_name))
    if not os.path.isfile(hf_name):
        hf = sns.pairplot(df2, hue='dataset', vars=vars_L, diag_kind='kde',plot_kws={'alpha':0.05})
        plt.suptitle(model_name)
        plt.tight_layout()
        hf.savefig(hf_name)
        plt.close(hf.fig)
        del hf

    hf_name = os.path.join(path_fig_ds, "empi_priors_{}.png".format(model_name))
    if not os.path.isfile(hf_name):
        hf = sns.pairplot(df2, vars=vars_L, diag_kind='kde', plot_kws={'alpha': 0.05})
        plt.suptitle(model_name)
        plt.tight_layout()
        hf.savefig(hf_name)
        plt.close(hf.fig)
        del hf

    del df2