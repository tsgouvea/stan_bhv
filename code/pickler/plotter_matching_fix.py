import os
import pickle

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns

stan_data = pickle.load(open(os.path.join('data','stan_matching_fix.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
dict_sess = stan_data.pop('dict_sess')
[stan_data.pop(i) for i in ['N','L','S']]
subj_per_sess = stan_data.pop('subj_per_sess')
pass


#df_fix = pickle.load(open(os.path.join('data','matching_fix.pickle'), 'rb'))
df_fix = pd.DataFrame(stan_data)

df_comp = pickle.load(open('data/model_compare.pickle', 'rb'))
print(df_comp)

#%% IDOBS
#TODO: fit initial value of piL/piR (for each session?)

d = pickle.load(open(os.path.join('data', 'optim', 'm004_matching_fix.pickle'), 'rb'))
optim_idobs = d['optim']
del d
df_fix.loc[:,'DV_idobs'] = optim_idobs['Ql_e'] - optim_idobs['Qr_e']

#%% SARSA
d = pickle.load(open(os.path.join('data', 'optim', 'm006_matching_fix.pickle'), 'rb'))
optim_sarsa = d['optim']
del d
#df_fix.loc[:,'DV_sarsa'] = np.multiply(optim_sarsa['beta'][stan_data['subj']], (optim_sarsa['Ql_sarsa'] - optim_sarsa['Qr_sarsa']))
df_fix.loc[:,'DV_sarsa'] = (optim_sarsa['Ql_sarsa'] - optim_sarsa['Qr_sarsa'])

#%%
hf0, ha0 = plt.subplots(1,2,figsize=(6,3))
plt.interactive(True)
#ha0[0].plot(optim['Ql_e'])
#ha0[0].plot(optim['Qr_e'])
#temp = pd.DataFrame()
ha0[0].hist(optim_idobs['eta'], histtype='step',label='eta')
#ha0[0].hist(optim_idobs['beta'], histtype='step', bins=100,label='beta')
plt.legend()

#ha0[1].plot(optim['Ql_sarsa'])
#ha0[1].plot(optim['Qr_sarsa'])
ha0[1].hist(optim_sarsa['alpha'], histtype='step', bins=100,label='alpha')
#ha0[1].hist(optim_sarsa['phi'], histtype='step', bins=100,label='phi')
#ha0[1].hist(optim_sarsa['beta'], histtype='step', bins=100,label='beta')

plt.legend()
#%%
hf, ha = plt.subplots(1,2,figsize=(6,3))
plt.interactive(True)
#ha[0].plot(optim['Ql_e'])
#ha[0].plot(optim['Qr_e'])
ha[0].hist(optim_idobs['Ql_e'], histtype='step', bins=100)
ha[0].hist(optim_idobs['Qr_e'], histtype='step', bins=100)

#ha[1].plot(optim['Ql_sarsa'])
#ha[1].plot(optim['Qr_sarsa'])
ha[1].hist(optim_sarsa['Ql_sarsa'], histtype='step', bins=100)
ha[1].hist(optim_sarsa['Qr_sarsa'], histtype='step', bins=100)

#%%

# ha[1].set_xlim(-1,1)




#%%

hf2, ha2 = plt.subplots(1,2,figsize=(6,3))

ha2[0].set_xlim(-1,1)
ha2[0].set_ylim(0,1)

ndx = (df_fix.loc[:,'DV_sarsa'] > 0) != (df_fix.loc[:,'DV_idobs'] > 0)
sns.regplot('DV_idobs', 'isChoiceLeft', data=df_fix.loc[ndx,:], x_bins=20, x_ci='ci',color='xkcd:leaf green',
            scatter=True, fit_reg=True, ci=None, units=None, order=1,
            logistic=True, lowess=False, robust=False, logx=False, x_partial=None,
            y_partial=None, truncate=False, dropna=True, x_jitter=None, y_jitter=None,
            label=None, marker='o', scatter_kws=None, line_kws={'label':'disagree'}, ax=ha2[0])
sns.regplot('DV_sarsa', 'isChoiceLeft', data=df_fix.loc[ndx,:], x_bins=20, x_ci='ci',color='xkcd:leaf green',
            scatter=True, fit_reg=True, ci=None, units=None, order=1,
            logistic=True, lowess=False, robust=False, logx=False, x_partial=None,
            y_partial=None, truncate=False, dropna=True, x_jitter=None, y_jitter=None,
            label=None, marker='o', scatter_kws=None, line_kws={'label':'disagree'}, ax=ha2[1])

ndx = (df_fix.loc[:,'DV_sarsa'] > 0) == (df_fix.loc[:,'DV_idobs'] > 0)
sns.regplot('DV_idobs', 'isChoiceLeft', data=df_fix.loc[ndx,:], x_bins=20, x_ci='ci',color='xkcd:brick red',
            scatter=True, fit_reg=True, ci=None, units=None, order=1,
            logistic=True, lowess=False, robust=False, logx=False, x_partial=None,
            y_partial=None, truncate=False, dropna=True, x_jitter=None, y_jitter=None,
            label=None, marker='o', scatter_kws=None, line_kws={'label':'agree'}, ax=ha2[0])
sns.regplot('DV_sarsa', 'isChoiceLeft', data=df_fix.loc[ndx,:], x_bins=20, x_ci='ci',color='xkcd:brick red',
            scatter=True, fit_reg=True, ci=None, units=None, order=1,
            logistic=True, lowess=False, robust=False, logx=False, x_partial=None,
            y_partial=None, truncate=False, dropna=True, x_jitter=None, y_jitter=None,
            label=None, marker='o', scatter_kws=None, line_kws={'label':'disagree'}, ax=ha2[1])

#%%
df_fix.loc[:,'DV_sarsa'].clip(lower=-3,upper=3,inplace=True)

# %%

x = sns.jointplot('DV_idobs', 'DV_sarsa', data=df_fix.loc[ndx,:],
              kind='hex',ylim=(-10,10))

#%%
#sns.kdeplot(df_fix.loc[ndx,'DV_idobs'],df_fix.loc[ndx,'DV_sarsa'],ax=ha[1])

hf3, ha3 = plt.subplots(1,2,figsize=(6,9))

set_subj = df_fix.subj.drop_duplicates().values

colors = sns.color_palette('colorblind',len(set_subj))

voff = 3
ivoff = 0
for isubj, subj in dict_subj.items():
    df_subj = df_fix.loc[df_fix.subj==isubj,:]
    list_sess = df_subj.sess.drop_duplicates().values
    for sess in list_sess:
        df_sess = df_subj.loc[df_subj.sess==sess,:].reset_index()
        ha3[0].plot(ivoff*voff+df_sess.DV_idobs,color=colors[isubj-1],alpha=.3)
        ha3[1].plot(ivoff * voff + df_sess.DV_sarsa,color=colors[isubj-1],alpha=.3)
    ivoff+=1