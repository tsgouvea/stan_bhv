import os
import pickle

import numpy as np
import pandas as pd

from tasks import dual2afc, lauglim

path_ds = os.path.join(os.sep,'Users','thiago','Neurodata','Datasets','matching_fix')
cherrypick_cols = ['isChoiceLeft', 'isChoiceRight', 'isRewarded','isBrokeFix','isEarlyWithdr',
                   'isLeftHi','isBaitLeft','isBaitRight','isChoiceBaited',
                   'stateTraj','waitingTime','reactionTime','movementTime']

fname_pickle = os.path.join('data','matching_fix.pickle')

for root, dirs, files in os.walk(path_ds):
    dirs.sort(reverse=False)
#     files.sort(reverse=True)
    ndxFiles = np.logical_and(['Matching'in n for n in files],[n.endswith('mat') for n in files])
    if any(ndxFiles):
        print(root)
        for file in np.array(files)[ndxFiles]:
            try:
                bhv = lauglim.parseSess(os.path.join(root,file))
                temp = bhv.parsedData.loc[:,cherrypick_cols].copy()
                temp.loc[:,'subj'] = bhv.bpod['Custom'].item()['Subject'].item()
                temp.loc[:,'sess'] = bhv.fname
                temp = pd.concat((temp,bhv.lauglim.dataX,bhv.lauglim.datay),axis=1)
#                 temp = temp.reset_index(level=0)
                matching_fix = pd.concat((matching_fix,temp)) if 'matching_fix' in locals() else temp
#                 bhv.dailyfig(fname_fig)
            except Exception as e:
#                 plt.close('all')
                print(file)
                print(e)
#                 print(traceback.format_exc())
matching_fix.reset_index(inplace=True)
with open(fname_pickle,'wb') as fhandle:
    pickle.dump(matching_fix,fhandle)
    
#%% FOR STAN

pickle_stan_data = os.path.join('data', 'stan_matching_fix.pickle')
    
ndxCol = ['isChoiceLeft', 'isRewarded','isChoiceBaited',
          'subj', 'sess','waitingTime', 'reactionTime', 'movementTime']
ndxRow = np.logical_and((matching_fix.isChoiceLeft | matching_fix.isChoiceRight), np.logical_not(matching_fix.isEarlyWithdr))
df = matching_fix.loc[ndxRow, ndxCol].copy()
df.loc[:, ['isChoiceLeft', 'isRewarded', 'isChoiceBaited']] = df.loc[:, ['isChoiceLeft', 'isRewarded', 'isChoiceBaited']].astype(int)
df_stan = df.copy()  # .iloc[10000:12000,:].copy()
dict_subj = {i + 1: n for i, n in enumerate(df_stan.subj.drop_duplicates().sort_values())}
for isubj, subj in dict_subj.items():
    df_stan.loc[df_stan.subj == subj, 'subj'] = isubj

dict_sess = {i + 1: n for i, n in enumerate(df_stan.sess.drop_duplicates().sort_values())}
for isess, sess in dict_sess.items():
    df_stan.loc[df_stan.sess == sess, 'sess'] = isess

stan_data = df_stan.to_dict(orient='list')
stan_data['N'] = df_stan.shape[0]
stan_data['L'] = len(dict_subj)
stan_data['S'] = len(dict_sess)
stan_data['subj_per_sess'] = df_stan.drop_duplicates(['subj', 'sess']).sort_values('sess').subj.values
stan_data['dict_subj'] = dict_subj
stan_data['dict_sess'] = dict_sess

with open(pickle_stan_data,'wb') as fhandle:
    pickle.dump(stan_data,fhandle)