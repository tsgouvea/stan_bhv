from locallib import StanModel_cache

#%% Model m002
# idobs
# hierarchical eta (subj,sess)

stan_code_m002 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  vector<lower=0,upper=1>[S] eta;
  vector<lower=0,upper=100>[L] a_subj;
  vector<lower=0,upper=100>[L] b_subj;
}
transformed parameters {
  vector[N] Ql_e;
  vector[N] Qr_e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_e[n] = 1-(1+exp(piL))^(-kL);
    Qr_e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      piL = piL - eta[sess[n]]*kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      kL = 1;
      kR += 1;
    }
    else {
      piR = piR  - eta[sess[n]]*kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  for (s in 1:S){
    eta[s] ~ beta(a_subj[subj_per_sess[s]],b_subj[subj_per_sess[s]]);
  }
  isChoiceLeft ~ bernoulli_logit((Ql_e-Qr_e));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | (Ql_e[n]-Qr_e[n]));
  }
}
"""
StanModel_cache(model_code=stan_code_m002,model_name='m002')

#%% Model m004
# idobs
# one eta per animal

stan_code_m004 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  vector<lower=0,upper=1>[L] eta;
}
transformed parameters {
  vector[N] Ql_e;
  vector[N] Qr_e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_e[n] = 1-(1+exp(piL))^(-kL);
    Qr_e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      temp = kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      piL = piL - eta[subj[n]]*temp;
      kL = 1;
      kR += 1;
    }
    else {
      temp = kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      piR = piR  - eta[subj[n]]*temp;
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  eta ~ beta(1,1);
  isChoiceLeft ~ bernoulli_logit((Ql_e-Qr_e));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | (Ql_e[n]-Qr_e[n]));
  }
}
"""
StanModel_cache(model_code=stan_code_m004,model_name='m004')

#%% Model m008
# idobs
# hierarchical eta (subj,sess) and beta

stan_code_m008 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  vector<lower=0,upper=1>[S] eta;
  vector<lower=0,upper=100>[L] a_subj;
  vector<lower=0,upper=100>[L] b_subj;
  vector<lower=0,upper=50>[S] beta;
  vector<lower=0,upper=100>[L] c_beta;
}
transformed parameters {
  vector[N] Ql_e;
  vector[N] Qr_e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_e[n] = 1-(1+exp(piL))^(-kL);
    Qr_e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      piL = piL - eta[sess[n]]*kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      kL = 1;
      kR += 1;
    }
    else {
      piR = piR  - eta[sess[n]]*kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  for (s in 1:S){
    eta[s] ~ beta(a_subj[subj_per_sess[s]],b_subj[subj_per_sess[s]]);
    beta[s] ~ cauchy(0,c_beta[subj_per_sess[s]]);
  }
  isChoiceLeft ~ bernoulli_logit(beta[sess].*(Ql_e-Qr_e));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | beta[sess[n]].*(Ql_e[n]-Qr_e[n]));
  }
}
"""

StanModel_cache(model_code=stan_code_m008,model_name='m008')

#%% Model m003
# SARSA
# One (alpha, phi, beta) per subj

stan_code_m003 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
}
parameters {
  real<lower=0,upper=1> alpha[L];
  real<lower=0,upper=1> phi[L]; // forgetting of unchosen option
  vector<lower=0,upper=50>[L] beta;
}
transformed parameters {
  vector[N] Ql_sarsa;
  vector[N] Qr_sarsa;
  for (n in 1:N-1){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      Ql_sarsa[n] = 0;
      Qr_sarsa[n] = 0;
    }
    if (isChoiceLeft[n]){
      Ql_sarsa[n+1]=Ql_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Ql_sarsa[n]);
      Qr_sarsa[n+1]=phi[subj[n]]*Qr_sarsa[n];
    }
    else {
      Qr_sarsa[n+1]=Qr_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Qr_sarsa[n]);
      Ql_sarsa[n+1]=phi[subj[n]]*Ql_sarsa[n];
    }    
  }  
}
model {
  beta ~ cauchy(0,5);
  for (l in 1:L){
    alpha[l] ~ uniform(0,1);
    phi[l] ~ uniform(0,1);
  }
  isChoiceLeft ~ bernoulli_logit(beta[subj].*(Ql_sarsa-Qr_sarsa));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | beta[subj[n]].*(Ql_sarsa[n]-Qr_sarsa[n]));
  }
}"""
StanModel_cache(model_code=stan_code_m003,model_name='m003')

#%% Model m005
# SARSA
# no beta

stan_code_m005 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
}
parameters {
  real<lower=0,upper=1> alpha[L];
  real<lower=0,upper=1> phi[L]; // forgetting of unchosen option
}
transformed parameters {
  vector[N] Ql_sarsa;
  vector[N] Qr_sarsa;
  for (n in 1:N-1){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      Ql_sarsa[n] = 0;
      Qr_sarsa[n] = 0;
    }
    if (isChoiceLeft[n]){
      Ql_sarsa[n+1]=Ql_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Ql_sarsa[n]);
      Qr_sarsa[n+1]=phi[subj[n]]*Qr_sarsa[n];
    }
    else {
      Qr_sarsa[n+1]=Qr_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Qr_sarsa[n]);
      Ql_sarsa[n+1]=phi[subj[n]]*Ql_sarsa[n];
    }    
  }  
}
model {
  for (l in 1:L){
    alpha[l] ~ uniform(0,1);
    phi[l] ~ uniform(0,1);
  }
  isChoiceLeft ~ bernoulli_logit((Ql_sarsa-Qr_sarsa));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | (Ql_sarsa[n]-Qr_sarsa[n]));
  }
}"""
StanModel_cache(model_code=stan_code_m005,model_name='m005')

#%% Model m006
# SARSA
# One beta per subj
# No forgetting

stan_code_m006 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
}
parameters {
  real<lower=0,upper=1> alpha[L];
  vector<lower=0,upper=100>[L] beta;
}
transformed parameters {
  vector[N] Ql_sarsa;
  vector[N] Qr_sarsa;
  for (n in 1:N-1){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      Ql_sarsa[n] = 0;
      Qr_sarsa[n] = 0;
    }
    if (isChoiceLeft[n]){
      Ql_sarsa[n+1]=Ql_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Ql_sarsa[n]);
      Qr_sarsa[n+1]=Qr_sarsa[n];
    }
    else {
      Qr_sarsa[n+1]=Qr_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Qr_sarsa[n]);
      Ql_sarsa[n+1]=Ql_sarsa[n];
    }    
  }  
}
model {
  beta ~ cauchy(0,50);
  for (l in 1:L){
    alpha[l] ~ uniform(0,1);
  }
  isChoiceLeft ~ bernoulli_logit(beta[subj].*(Ql_sarsa-Qr_sarsa));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | beta[subj[n]].*(Ql_sarsa[n]-Qr_sarsa[n]));
  }
}"""
StanModel_cache(model_code=stan_code_m006,model_name='m006')

#%% Model m007
# SARSA
# One (alpha, phi, beta) per sess, hierarchically per subj

stan_code_m007 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  vector<lower=0,upper=1>[S] alpha;
  vector<lower=0,upper=1>[S] phi; // forgetting of unchosen option
  vector<lower=0,upper=50>[S] beta;
  vector<lower=0,upper=100>[L] a_alpha;
  vector<lower=0,upper=100>[L] b_alpha;
  vector<lower=0,upper=100>[L] a_phi;
  vector<lower=0,upper=100>[L] b_phi;
  vector<lower=0,upper=100>[L] c_beta;
}
transformed parameters {
  vector[N] Ql_sarsa;
  vector[N] Qr_sarsa;
  for (n in 1:N-1){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      Ql_sarsa[n] = 0;
      Qr_sarsa[n] = 0;
    }
    if (isChoiceLeft[n]){
      Ql_sarsa[n+1]=Ql_sarsa[n]+alpha[sess[n]]*(isRewarded[n]-Ql_sarsa[n]);
      Qr_sarsa[n+1]=phi[sess[n]]*Qr_sarsa[n];
    }
    else {
      Qr_sarsa[n+1]=Qr_sarsa[n]+alpha[sess[n]]*(isRewarded[n]-Qr_sarsa[n]);
      Ql_sarsa[n+1]=phi[sess[n]]*Ql_sarsa[n];
    }    
  }  
}
model {
  for (s in 1:S){
    alpha[s] ~ beta(a_alpha[subj_per_sess[s]],b_alpha[subj_per_sess[s]]);
    phi[s] ~ beta(a_phi[subj_per_sess[s]],b_phi[subj_per_sess[s]]);
    beta[s] ~ cauchy(0,c_beta[subj_per_sess[s]]);
  }
  isChoiceLeft ~ bernoulli_logit(beta[sess].*(Ql_sarsa-Qr_sarsa));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | beta[sess[n]].*(Ql_sarsa[n]-Qr_sarsa[n]));
  }
}"""
StanModel_cache(model_code=stan_code_m007,model_name='m007')

#%% Model m009
# SARSA
# no beta, no phi

stan_code_m009 = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
}
parameters {
  real<lower=0,upper=1> alpha[L];
}
transformed parameters {
  vector[N] Ql_sarsa;
  vector[N] Qr_sarsa;
  for (n in 1:N-1){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      Ql_sarsa[n] = 0;
      Qr_sarsa[n] = 0;
    }
    if (isChoiceLeft[n]){
      Ql_sarsa[n+1]=Ql_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Ql_sarsa[n]);
      Qr_sarsa[n+1]=Qr_sarsa[n];
    }
    else {
      Qr_sarsa[n+1]=Qr_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Qr_sarsa[n]);
      Ql_sarsa[n+1]=Ql_sarsa[n];
    }    
  }  
}
model {
  for (l in 1:L){
    alpha[l] ~ uniform(0,1);
  }
  isChoiceLeft ~ bernoulli_logit((Ql_sarsa-Qr_sarsa));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | (Ql_sarsa[n]-Qr_sarsa[n]));
  }
}"""
StanModel_cache(model_code=stan_code_m009,model_name='m009')