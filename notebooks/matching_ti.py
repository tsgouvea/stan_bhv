import os
import pickle

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import pystan

# %%
datasets = ['matching_cdda2c5'] #['matching_conf', 'matching_fix']
models = ['m{:03d}'.format(n) for n in [207,211,212]]# [3, 107, 111, 112]]

nbins = 8
fit_reg = True
y_jitter = 0.1

# %%
path_data = os.path.join(os.getcwd(), 'data')

for dataset in datasets:

    fname_data = os.path.join(path_data, 'stan_{}.pickle'.format(dataset))
    stan_data = pickle.load(open(fname_data, 'rb'))
    dict_subj = stan_data.pop('dict_subj')
    dict_sess = stan_data.pop('dict_sess')
    hf = {}
    ha = {}

    for imodel, model_name in enumerate(models):
        # %%
        fname_optim = os.path.join('data', 'optim', 'optim_{}_{}.pickle'.format(model_name, dataset))
        d = pickle.load(open(fname_optim, 'rb'))
        optim = d['optim']
        del d
        # %%
        df_data = pd.DataFrame({n: np.array(stan_data[n]) for n in
                                ['isChoiceLeft', 'isRewarded','isChoiceBaited', 'subj', 'sess', 'waitingTime', 'reactionTime',
                                 'movementTime']})
        # if model_name == 'm003':
        #     for var in ['Ql', 'Qr']:
        #         df_data.loc[:, var] = 1e-10 + optim[
        #             np.array([n for n in optim.keys()])[[n.startswith(var) for n in optim.keys()]].item()]
        #     df_data.loc[:, 'greedy'] = ((df_data.loc[:, 'Ql'] > df_data.loc[:, 'Qr']) == df_data.loc[:, 'isChoiceLeft'])
        #     df_data.loc[:, 'dv'] = optim['beta'][np.array(stan_data['subj']) - 1] * np.log(
        #         df_data.loc[:, 'Ql'] / df_data.loc[:, 'Qr'])
        # elif model_name == 'm107' or model_name == 'm111':
        #     for var in ['Ql', 'Qr']:
        #         df_data.loc[:, var] = 1e-10 + optim[
        #             np.array([n for n in optim.keys()])[[n.startswith(var) for n in optim.keys()]].item()]
        #     df_data.loc[:, 'greedy'] = ((df_data.loc[:, 'Ql'] > df_data.loc[:, 'Qr']) == df_data.loc[:, 'isChoiceLeft'])
        #     df_data.loc[:, 'dv'] = optim['beta'][np.array(stan_data['sess']) - 1] * np.log(
        #         df_data.loc[:, 'Ql'] / df_data.loc[:, 'Qr'])
        # elif model_name == 'm112':
        #     df_data.loc[:, 'dv'] = optim['w'][np.array(stan_data['sess']) - 1] * optim['betai'][np.array(stan_data['sess']) - 1] * np.log((1e-10 +optim['Ql_e'])/(1e-10 +optim['Qr_e'])) + (1-optim['w'][np.array(stan_data['sess']) - 1]) * optim['betas'][np.array(stan_data['sess']) - 1] * np.log(
        #                 (1e-10 + optim['Ql_sarsa']) / (1e-10 + optim['Qr_sarsa']))
        #     df_data.loc[:,'greedy'] = (df_data.loc[:, 'dv'] > 0) == df_data.loc[:, 'isChoiceLeft']
        # df_data.loc[:, 'dv'] = np.clip(df_data.loc[:, 'dv'],-10,10)
        # df_data.loc[:, 'abs_dv'] = abs(df_data.loc[:, 'dv'])
        df_data.loc[:, 'dv'] = optim['dv']
        df_data.loc[:, 'greedy'] = (df_data.loc[:, 'dv'] > 0) == df_data.loc[:, 'isChoiceLeft']
        # df_data.loc[:, 'dv'] = np.clip(df_data.loc[:, 'dv'], -10, 10)
        df_data.loc[:, 'abs_dv'] = abs(df_data.loc[:, 'dv'])
        # %%
        for isubj, subj in dict_subj.items():
            try:
                df_data_subj = df_data.loc[df_data.subj == isubj, :].copy()
                if subj not in hf.keys():
                    hf[subj], ha[subj] = plt.subplots(len(models), 4, figsize=(8,2*len(models)), sharex='col', sharey='col')
                sns.regplot(x='dv', y='isChoiceLeft', ax=ha[subj][imodel, 0], data=df_data_subj,
                            color='xkcd:black', y_jitter=y_jitter, x_bins=nbins, logistic=True, ci=None)
                ha[subj][imodel, 0].set_ylim(-.1,1.1)
                ha[subj][imodel, 0].set_title(model_name)
                label = 'greedy' if imodel == 0 else None
                sns.regplot(x='abs_dv', y='waitingTime', ax=ha[subj][imodel, 3],
                            data=df_data_subj.loc[np.logical_and(df_data_subj.greedy,
                                                                 np.logical_not(df_data_subj.isChoiceBaited)), :],
                            color='xkcd:leaf green', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None,
                            label=label)
                label = 'not greedy' if imodel == 0 else None
                sns.regplot(x='abs_dv', y='waitingTime', ax=ha[subj][imodel, 3],
                            data=df_data_subj.loc[np.logical_and(np.logical_not(df_data_subj.greedy),
                                                                 np.logical_not(df_data_subj.isChoiceBaited)), :],
                            color='xkcd:brick', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None,
                            label=label)
                if imodel == 0:
                    ha[subj][imodel,3].legend(fontsize='x-small',frameon=False)

                for ivar, var in enumerate(['reactionTime','movementTime']):
                    sns.regplot(x='abs_dv', y=var, ax=ha[subj][imodel, ivar+1],
                                data=df_data_subj.loc[df_data_subj.greedy, :],
                                color='xkcd:leaf green', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None)

                    sns.regplot(x='abs_dv', y=var, ax=ha[subj][imodel, ivar+1],
                                data=df_data_subj.loc[np.logical_not(df_data_subj.greedy), :],
                                color='xkcd:brick', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None)

                plt.suptitle(subj)
                plt.tight_layout(rect=[0, 0, 1, 0.95])
                hf[subj].savefig(os.path.join('notebooks', 'Matching_TI', "dv_{}_{}.png".format(dataset, subj)))
            except Exception as e:
                print(e)

