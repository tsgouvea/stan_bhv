import os
import pickle

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import pystan

# %%
datasets = ['matching_cdda2c5','matching_conf'] #['matching_conf', 'matching_fix']
models = ['m{:03d}'.format(n) for n in [207,211,212]]# [3, 107, 111, 112]]

nbins = 8
fit_reg = True
y_jitter = 0.1

# %%
path_data = os.path.join(os.getcwd(), 'data')

for dataset in datasets:

    fname_data = os.path.join(path_data, 'stan_{}.pickle'.format(dataset))
    stan_data = pickle.load(open(fname_data, 'rb'))
    if dataset == 'matching_conf':
        cherrypick = ['TG024','TG025','TG026']
        dict_subj_temp = stan_data.pop('dict_subj')
        dict_subj = {key:value for key, value in dict_subj_temp.items() if value in cherrypick}
        dict_sess = stan_data.pop('dict_sess')
    else:
        dict_subj = stan_data.pop('dict_subj')
    hf = {}
    ha = {}

    for imodel, model_name in enumerate(models):
        # %%
        fname_optim = os.path.join('data', 'optim', 'optim_{}_{}.pickle'.format(model_name, dataset))
        d = pickle.load(open(fname_optim, 'rb'))
        optim = d['optim']
        del d
        # %%
        df_data = pd.DataFrame({n: np.array(stan_data[n]) for n in
                                ['isChoiceLeft', 'isRewarded','isChoiceBaited', 'subj', 'sess', 'waitingTime', 'reactionTime',
                                 'movementTime']})
        df_data.loc[:, 'dv'] = optim['dv']
        df_data.loc[:, 'greedy'] = (df_data.loc[:, 'dv'] > 0) == df_data.loc[:, 'isChoiceLeft']
        df_data.loc[:, 'abs_dv'] = abs(df_data.loc[:, 'dv'])
        # %%
        for isubj, subj in dict_subj.items():
            if dataset == 'matching_conf' and not subj.startswith('TG'):
                continue
            try:
                df_data_subj = df_data.loc[df_data.subj == isubj, :].copy()
                if model_name not in hf.keys():
                    hf[model_name], ha[model_name] = plt.subplots(len(dict_subj), 4, figsize=(8,2*len(dict_subj)))
                sns.regplot(x='dv', y='isChoiceLeft', ax=ha[model_name][isubj-min(dict_subj.keys()), 0], data=df_data_subj,
                            color='xkcd:black', y_jitter=y_jitter, x_bins=nbins, logistic=True, ci=None)
                ha[model_name][isubj-min(dict_subj.keys()), 0].set_ylim(-.1,1.1)
                ha[model_name][isubj-min(dict_subj.keys()), 0].set_title(subj)
                label = 'greedy' if imodel == 0 else None
                sns.regplot(x='abs_dv', y='waitingTime', ax=ha[model_name][isubj-min(dict_subj.keys()), 3],
                            data=df_data_subj.loc[np.logical_and(df_data_subj.greedy,
                                                                 np.logical_not(df_data_subj.isChoiceBaited)), :],
                            color='xkcd:leaf green', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None,
                            label=label)
                label = 'not greedy' if imodel == 0 else None
                sns.regplot(x='abs_dv', y='waitingTime', ax=ha[model_name][isubj-min(dict_subj.keys()), 3],
                            data=df_data_subj.loc[np.logical_and(np.logical_not(df_data_subj.greedy),
                                                                 np.logical_not(df_data_subj.isChoiceBaited)), :],
                            color='xkcd:brick', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None,
                            label=label)
                if imodel == 0:
                    ha[model_name][isubj-min(dict_subj.keys()),3].legend(fontsize='x-small',frameon=False)

                for ivar, var in enumerate(['reactionTime','movementTime']):
                    sns.regplot(x='abs_dv', y=var, ax=ha[model_name][isubj-min(dict_subj.keys()), ivar+1],
                                data=df_data_subj.loc[df_data_subj.greedy, :],
                                color='xkcd:leaf green', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None)

                    sns.regplot(x='abs_dv', y=var, ax=ha[model_name][isubj-min(dict_subj.keys()), ivar+1],
                                data=df_data_subj.loc[np.logical_not(df_data_subj.greedy), :],
                                color='xkcd:brick', y_jitter=y_jitter, x_bins=nbins, fit_reg=fit_reg, ci=None)

                plt.suptitle(model_name)
                plt.tight_layout(rect=[0, 0, 1, 0.95])
                hf[model_name].savefig(os.path.join('notebooks', 'Matching_TI', "dv_{}_{}.png".format(dataset, model_name)))
            except Exception as e:
                print(e)

