import os
from warnings import warn

import pystan
import pickle
from hashlib import md5
import numpy as np
#%%
def StanModel_cache(model_code, model_name=None, **kwargs):
    """Use just as you would `stan`.
    From https://pystan.readthedocs.io/en/latest/avoiding_recompilation.html"""
    code_hash = md5(model_code.encode('ascii')).hexdigest()
    if model_name is None:
        cache_fn = 'cached-model-{}.pkl'.format(code_hash)
    else:
        cache_fn = 'cached-{}-{}.pkl'.format(model_name, code_hash)
    try:
        sm = pickle.load(open(os.path.join('models', cache_fn), 'rb'))
    except:
        sm = pystan.StanModel(model_code=model_code, **kwargs)
        with open(os.path.join('models', cache_fn), 'wb') as f:
            pickle.dump(sm, f)
    else:
        print("Using cached StanModel end ", code_hash[-4:])
    return sm
#%%
def fit(dataset='matching_fix',model_name='m002',path_models=None,path_data=None,path_out=os.path.join('data', 'fit'),
        warmup=None, iter=None,chains=3):
    iter = 2000 if iter==None else iter
    warmup = int(iter/2) if warmup == None else warmup
    path_data = os.path.join(os.getcwd(),'data') if path_data == None else path_data
    path_models = os.path.join(os.getcwd(), 'models') if path_models == None else path_models
    fname_sm = os.path.join(path_models, np.array(os.listdir(path_models))[[model_name in n for n in os.listdir(path_models)]].item())
    # TODO: handle case there's more than one model with same name
    sm = pickle.load(open(fname_sm,'rb'))
    fname_data = os.path.join(path_data,'stan_{}.pickle'.format(dataset))
    stan_data = pickle.load(open(fname_data, 'rb'))
    dict_subj = stan_data.pop('dict_subj')
    dict_sess = stan_data.pop('dict_sess')
    os.makedirs(path_out,exist_ok=True)
    fname_fit = os.path.join(path_out, 'fit_{}_{}.pickle'.format(model_name,dataset))
    if not os.path.isfile(fname_fit):
        fitobj = sm.sampling(data=stan_data, warmup=warmup, iter=iter, chains=chains, verbose=True)
        pickle.dump({'model': sm, 'fit': fitobj}, open(fname_fit, 'wb'), protocol=-1)
#%%
def optim(dataset='matching_fix',model_name='m002',path_models=None,path_data=None,path_out=os.path.join('data', 'optim'),init='random',**kwargs):
    path_data = os.path.join(os.getcwd(), 'data') if path_data == None else path_data
    path_models = os.path.join(os.getcwd(), 'models') if path_models == None else path_models
    fname_sm = os.path.join(path_models, np.array(os.listdir(path_models))[[model_name in n for n in os.listdir(path_models)]].item())
    sm = pickle.load(open(fname_sm,'rb'))
    fname_data = os.path.join(path_data,'stan_{}.pickle'.format(dataset))
    stan_data = pickle.load(open(fname_data, 'rb'))
    dict_subj = stan_data.pop('dict_subj')
    dict_sess = stan_data.pop('dict_sess')
    os.makedirs(path_out,exist_ok=True)
    fname_optim = os.path.join(path_out, 'optim_{}_{}.pickle'.format(model_name,dataset))
    if not os.path.isfile(fname_optim):
        if init=='fit':
            try:
                fname_fit = os.path.join('data', 'fit', 'fit_{}_{}.pickle'.format(model_name, dataset))
                d = pickle.load(open(fname_fit, 'rb'))
                fitobj = d['fit']
                del d
                flatnames = np.array(fitobj.flatnames)
                flatnames = list(flatnames[[not n.startswith('Q') and not n.startswith('log') for n in flatnames]])
                flatnames = np.unique([n.split('[')[0] for n in flatnames])
                extr = fitobj.extract(pars=flatnames, permuted=True, inc_warmup=False)
                init = {}
                for ivar, var in enumerate(flatnames):
                    print(ivar, var)
                    temp = extr[var]
                    init[var] = np.mean(extr[var][:, :], axis=0) if len(extr[var].shape) > 1 else np.mean(extr[var])
            except Exception as e:
                warn('Failed to initialize optimization from fit object - using random init instead.\nException raised: {}'.format(e))
                init = 'random'
        try:
            # print("init: {}".format(init))
            optimobj = sm.optimizing(data=stan_data, verbose=True,init=init,**kwargs)
        except RuntimeError as e:
            print(e)
            print("LBFGS failed. Trying BFGS.")
            try:
                optimobj = sm.optimizing(data=stan_data, verbose=True, algorithm='BFGS', init=init,**kwargs)
            except RuntimeError as e:
                print(e)
                print("Both LBFGS and BFGS failed. Trying Newton method.")
                optimobj = sm.optimizing(data=stan_data, verbose=True, algorithm='Newton', init=init,**kwargs)
            # # return

        pickle.dump({'model': sm, 'optim': optimobj}, open(fname_optim, 'wb'), protocol=-1)