#!/usr/bin/env python
# coding: utf-8

# **from ewa_data_009_idobs-Copy2**
# 
# - generate log_likelihood as in [https://www.gitmemory.com/issue/arviz-devs/arviz/668/492486535]
# - might need to move computation of Q to transformed_parameters

import os
import pickle

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import pystan
import seaborn as sns
import scipy.stats as spt
import arviz as az

import sys
print(sys.version)


## Stan Data

#path_ds = os.path.join('/media','thiagoatserver','Neurodata','Datasets','matching_fix')
path_ds = os.path.join('/Users','thiago','Neurodata','Datasets','matching_fix')

fname_pickle = os.path.join(path_ds,'df_big.pickle')
if os.path.isfile(fname_pickle):
    with open(fname_pickle,'rb') as fhandle:
        df_big = pickle.load(fhandle)

ndxCol = ['isChoiceLeft', 'isRewarded',
       'idobs_pLeft', 'idobs_pRight', 'subj', 'sess',
       'waitingTime', 'reactionTime', 'movementTime']
ndxRow = np.logical_and((df_big.isChoiceLeft | df_big.isChoiceRight),np.logical_not(df_big.isEarlyWithdr))
df = df_big.loc[ndxRow,ndxCol]
df.loc[:,['isChoiceLeft', 'isRewarded']]=df.loc[:,['isChoiceLeft', 'isRewarded']].astype(int)
df_stan = df.copy()#.iloc[10000:12000,:].copy()
dict_subj = {i+1:n for i,n in enumerate(df_stan.subj.drop_duplicates().sort_values())}
for isubj,subj in dict_subj.items():
    df_stan.loc[df_stan.subj==subj,'subj']=isubj

dict_sess = {i+1:n for i,n in enumerate(df_stan.sess.drop_duplicates().sort_values())}
for isess,sess in dict_sess.items():
    df_stan.loc[df_stan.sess==sess,'sess']=isess

stan_data = df_stan.to_dict(orient='list')
stan_data['N'] = df_stan.shape[0]
stan_data['L'] = len(dict_subj)
stan_data['S'] = len(dict_sess)
stan_data['subj_per_sess'] = df_stan.drop_duplicates(['subj','sess']).sort_values('sess').subj.values

# Pre-fit analyses

fs = 15
hf, ha = plt.subplots(len(dict_subj),1,figsize=(14,14),sharex=True,sharey=True)

for isubj,subj in enumerate(df.subj.drop_duplicates().sort_values()):
    df_sub = df.loc[df.subj==subj,:].copy().reset_index()
    for isess,sess in enumerate(df_sub.sess.drop_duplicates().sort_values()):
        df_sess = df_sub.loc[df_sub.sess==sess,:].dropna().copy()
        ha[isubj].scatter(isess,df_sess.isRewarded.mean())
        if isess == 0:
            ha[isubj].set_title(subj,fontsize=fs)

plt.tight_layout()

# # Stan Code

# In[3]:

stan_data.keys()

# decision completely controlled by learn_pi estimate of reward probability

# ## stan_code_idobs

# ### without Beta, hierarchical Eta

# In[4]:


stan_code_e = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  vector<lower=0.0001,upper=1>[S] eta;
  vector<lower=0,upper=100>[L] a_subj;
  vector<lower=0,upper=100>[L] b_subj;
}
transformed parameters {
  vector[N] Ql_e;
  vector[N] Qr_e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_e[n] = 1-(1+exp(piL))^(-kL);
    Qr_e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      temp = kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      piL = piL - eta[sess[n]]*temp;
      kL = 1;
      kR += 1;
    }
    else {
      temp = kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      piR = piR  - eta[sess[n]]*temp;
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  a_subj ~ normal(5,1);
  b_subj ~ cauchy(0,10);
  for (s in 1:S){
    eta[s] ~ beta(a_subj[subj_per_sess[s]],b_subj[subj_per_sess[s]]);
  }
  isChoiceLeft ~ bernoulli_logit((Ql_e-Qr_e));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | (Ql_e[n]-Qr_e[n]));
  }
}
"""

sm_e = pystan.StanModel(model_code=stan_code_e)


# In[ ]:


fit_e = sm_e.sampling(data=stan_data, iter=1000, chains=6,verbose=True)

# fit_e


# In[ ]:


fit_e


# ## 1 eta per animal

# In[ ]:


stan_code_1e = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
}
parameters {
  vector<lower=0.0001,upper=1>[L] eta;
}
transformed parameters {
  vector[N] Ql_1e;
  vector[N] Qr_1e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_1e[n] = 1-(1+exp(piL))^(-kL);
    Qr_1e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      temp = kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      piL = piL - eta[subj[n]]*temp;
      kL = 1;
      kR += 1;
    }
    else {
      temp = kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      piR = piR  - eta[subj[n]]*temp;
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  eta ~ beta(2,1);
  isChoiceLeft ~ bernoulli_logit((Ql_1e-Qr_1e));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | (Ql_1e[n]-Qr_1e[n]));
  }
}
"""
sm_1e = pystan.StanModel(model_code=stan_code_1e)


# In[ ]:


fit_1e = sm_1e.sampling(data=stan_data, iter=5000, chains=6,verbose=True)


# In[ ]:


fit_1e


# ### with Beta

# In[ ]:

stan_code_be = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  vector<lower=0.0001,upper=1>[S] eta;
  vector<lower=0,upper=100>[L] a_subj;
  vector<lower=0,upper=100>[L] b_subj;
  vector<lower=0,upper=100>[S] beta;
  vector<lower=0,upper=100>[L] beta_subj;
}
transformed parameters {
  vector[N] Ql_be;
  vector[N] Qr_be;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_be[n] = 1-(1+exp(piL))^(-kL);
    Qr_be[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      temp = kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      piL = piL - eta[subj[n]]*temp;
      kL = 1;
      kR += 1;
    }
    else {
      temp = kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      piR = piR  - eta[subj[n]]*temp;
      kL += 1;
      kR = 1;
    }    
  }
}
model {
  beta_subj ~ cauchy(0,3);
  a_subj ~ normal(5,1);
  b_subj ~ cauchy(0,10);
  for (s in 1:S){
    eta[s] ~ beta(a_subj[subj_per_sess[s]],b_subj[subj_per_sess[s]]);
    beta[s] ~ cauchy(0,beta_subj[subj_per_sess[s]]);
  }
  isChoiceLeft ~ bernoulli_logit(beta[subj].*(Ql_be-Qr_be));
}
generated quantities {
  vector[N] log_likelihood;
  for (n in 1:N){
    log_likelihood[n] = bernoulli_logit_lpmf(isChoiceLeft[n] | beta[subj[n]].*(Ql_be[n]-Qr_be[n]));
  }
}
"""
sm_be = pystan.StanModel(model_code=stan_code_be)


# In[ ]:


fit_be = sm_be.sampling(data=stan_data, iter=5000, chains=6,verbose=True)

# 


# In[ ]:


fit_be


# fit_be.plot()

# In[ ]:


dataset_dict = {'fit_e':az.from_pystan(fit_e,log_likelihood='log_likelihood'),
                'fit_1e':az.from_pystan(fit_1e,log_likelihood='log_likelihood'),
                'fit_be':az.from_pystan(fit_be,log_likelihood='log_likelihood')}


# In[ ]:


df_compare = az.compare(dataset_dict)


# In[ ]:


df_compare


# In[ ]:





# In[ ]:




