# %%

import pickle
import os

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from locallib import StanModel_cache

# %%

stan_data = pickle.load(open(os.path.join('data', 'stan_data.pickle'), 'rb'))
dict_subj = stan_data.pop('dict_subj')
df_data = pd.DataFrame({n: np.array(stan_data[n]) for n in ['isChoiceLeft', 'isRewarded', 'subj', 'sess', 'waitingTime', 'reactionTime', 'movementTime']})

# %%

fname_fit_m002 = os.path.join('data', 'fit_m002_matching_fix.pickle')
d = pickle.load(open(fname_fit_m002, 'rb'))
fit_m002 = d['fit']
del d
df_fit_m002 = fit_m002.to_dataframe()

myvars = np.array(fit_m002.flatnames)[[not n.startswith('log_like') and not n.startswith('Q') for n in fit_m002.flatnames]]

pointEst = df_fit_m002.loc[:,['eta[1]', 'eta[2]', 'eta[3]', 'eta[4]', 'eta[5]', 'eta[6]',
       'eta[7]', 'eta[8]', 'eta[9]', 'eta[10]', 'eta[11]', 'eta[12]',
       'eta[13]', 'eta[14]', 'eta[15]', 'eta[16]', 'eta[17]', 'eta[18]',
       'eta[19]', 'eta[20]', 'eta[21]', 'eta[22]', 'eta[23]', 'eta[24]',
       'eta[25]', 'eta[26]', 'eta[27]', 'eta[28]', 'eta[29]', 'eta[30]',
       'eta[31]', 'eta[32]', 'eta[33]', 'eta[34]', 'eta[35]', 'eta[36]',
       'eta[37]', 'eta[38]', 'eta[39]', 'eta[40]', 'eta[41]', 'eta[42]',
       'eta[43]', 'eta[44]', 'eta[45]', 'eta[46]', 'eta[47]', 'eta[48]',
       'eta[49]', 'eta[50]', 'eta[51]', 'eta[52]', 'eta[53]', 'eta[54]',
       'eta[55]', 'eta[56]', 'eta[57]', 'eta[58]', 'eta[59]', 'eta[60]',
       'eta[61]', 'a_subj[1]', 'a_subj[2]', 'a_subj[3]', 'a_subj[4]',
       'a_subj[5]', 'a_subj[6]', 'a_subj[7]', 'a_subj[8]', 'b_subj[1]',
       'b_subj[2]', 'b_subj[3]', 'b_subj[4]', 'b_subj[5]', 'b_subj[6]',
       'b_subj[7]', 'b_subj[8]']].mean()

stan_data['a_subj'] = pointEst.loc[[n.startswith('a_subj') for n in pointEst.index]].values
stan_data['b_subj'] = pointEst.loc[[n.startswith('b_subj') for n in pointEst.index]].values
stan_data['eta'] = pointEst.loc[[n.startswith('eta') for n in pointEst.index]].values

stan_code_m002gen = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
  vector<lower=0,upper=1>[S] eta;
  vector<lower=0,upper=100>[L] a_subj;
  vector<lower=0,upper=100>[L] b_subj;
}
model {
}
generated quantities {
  vector[N] Ql_e;
  vector[N] Qr_e;
  real kL = 1;
  real kR = 1;
  real piL;
  real piR;
  real temp;
  for (n in 1:N){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      piL = 0;
      piR = 0;
      kL = 1;
      kR = 1;
    }
    Ql_e[n] = 1-(1+exp(piL))^(-kL);
    Qr_e[n] = 1-(1+exp(piR))^(-kR);
    if (isChoiceLeft[n]){
      temp = kL*exp(piL)/(1+exp(piL))*((isRewarded[n]/(1-(1+exp(piL))^kL)) + (1-isRewarded[n]));
      piL = piL - eta[sess[n]]*temp;
      kL = 1;
      kR += 1;
    }
    else {
      temp = kR*exp(piR)/(1+exp(piR))*(isRewarded[n]*(1/(1-(1+exp(piR))^kR)) + (1-isRewarded[n]));
      piR = piR  - eta[sess[n]]*temp;
      kL += 1;
      kR = 1;
    }    
  }
}
"""
sm_m002gen = StanModel_cache(model_code=stan_code_m002gen,model_name='m002gen')
fit_m002gen = sm_m002gen.sampling(data=stan_data, iter=1, chains=1,verbose=True,algorithm="Fixed_param")
df_m002gen = fit_m002gen.to_dataframe()
df_data.loc[:,'Ql_idobs'] = df_m002gen.loc[:,[n.startswith('Ql') for n in df_m002gen.columns]].values.ravel()
df_data.loc[:,'Qr_idobs'] = df_m002gen.loc[:,[n.startswith('Qr') for n in df_m002gen.columns]].values.ravel()

# %%

fname_fit_m003 = os.path.join('data', 'fit_m003.pickle')
d = pickle.load(open(fname_fit_m003, 'rb'))
fit_m003 = d['fit']
del d
df_fit_m003 = fit_m003.to_dataframe()

pointEst = df_fit_m003.loc[:,['alpha[1]', 'alpha[2]', 'alpha[3]',
       'alpha[4]', 'alpha[5]', 'alpha[6]', 'alpha[7]', 'alpha[8]', 'phi[1]',
       'phi[2]', 'phi[3]', 'phi[4]', 'phi[5]', 'phi[6]', 'phi[7]', 'phi[8]',
       'beta[1]', 'beta[2]', 'beta[3]', 'beta[4]', 'beta[5]', 'beta[6]',
       'beta[7]', 'beta[8]']].mean()

stan_data['alpha'] = pointEst.loc[[n.startswith('alpha') for n in pointEst.index]].values
stan_data['beta'] = pointEst.loc[[n.startswith('beta') for n in pointEst.index]].values
stan_data['phi'] = pointEst.loc[[n.startswith('phi') for n in pointEst.index]].values

stan_code_m003gen ="""data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  real<lower=0,upper=1> alpha[L];
  real<lower=0,upper=1> phi[L]; // forgetting of unchosen option
  vector<lower=0,upper=100>[L] beta;
}
model {
}
generated quantities {
  vector[N] Ql_sarsa;
  vector[N] Qr_sarsa;
  for (n in 1:N-1){
    if (n == 1 || subj[n] != subj[n-1] || sess[n] != sess[n-1]){
      Ql_sarsa[n] = 0;
      Qr_sarsa[n] = 0;
    }
    if (isChoiceLeft[n]){
      Ql_sarsa[n+1]=Ql_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Ql_sarsa[n]);
      Qr_sarsa[n+1]=phi[subj[n]]*Qr_sarsa[n];
    }
    else {
      Qr_sarsa[n+1]=Qr_sarsa[n]+alpha[subj[n]]*(isRewarded[n]-Qr_sarsa[n]);
      Ql_sarsa[n+1]=phi[subj[n]]*Ql_sarsa[n];
    }    
  }
}
"""
sm_m003gen = StanModel_cache(model_code=stan_code_m003gen,model_name='m003gen')
fit_m003gen = sm_m003gen.sampling(data=stan_data, iter=1, chains=1,verbose=True,algorithm="Fixed_param")
df_m003gen = fit_m003gen.to_dataframe()
df_data.loc[:,'Ql_sarsa'] = df_m003gen.loc[:,[n.startswith('Ql') for n in df_m003gen.columns]].values.ravel()
df_data.loc[:,'Qr_sarsa'] = df_m003gen.loc[:,[n.startswith('Qr') for n in df_m003gen.columns]].values.ravel()

#%%
df_data.loc[:,'greedy_sarsa'] = ((df_data.loc[:,'Ql_sarsa'] > df_data.loc[:,'Qr_sarsa']) == df_data.loc[:,'isChoiceLeft'])
df_data.loc[:,'greedy_idobs'] = ((df_data.loc[:,'Ql_idobs'] > df_data.loc[:,'Qr_idobs']) == df_data.loc[:,'isChoiceLeft'])

df_data.loc[:,'dv_sarsa'] = (df_data.loc[:,'Ql_sarsa'] - df_data.loc[:,'Qr_sarsa'])
df_data.loc[:,'dv_idobs'] = (df_data.loc[:,'Ql_idobs'] - df_data.loc[:,'Qr_idobs'])

#%%
# plt.interactive(False)
#
nbins = 10
fit_reg = False
y_jitter = 0.1
for isubj,subj in dict_subj.items():
    df_data_subj = df_data.loc[df_data.subj == isubj,:].copy()
    hf, ha = plt.subplots(2,2)

    #%% Choice
    sns.regplot(x='dv_sarsa',y='isChoiceLeft',ax=ha[0,0],data=df_data_subj.loc[df_data_subj.greedy_sarsa,:],
                color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    sns.regplot(x='dv_sarsa',y='isChoiceLeft',ax=ha[0,0],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_sarsa),:],
                color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    sns.regplot(x='dv_idobs',y='isChoiceLeft',ax=ha[0,1],data=df_data_subj.loc[df_data_subj.greedy_idobs,:],
                color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    sns.regplot(x='dv_idobs',y='isChoiceLeft',ax=ha[0,1],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_idobs),:],
                color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    #%% WT
    sns.regplot(x='dv_sarsa',y='waitingTime',ax=ha[1,0],data=df_data_subj.loc[df_data_subj.greedy_sarsa,:],
                color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    sns.regplot(x='dv_sarsa',y='waitingTime',ax=ha[1,0],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_sarsa),:],
                color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    sns.regplot(x='dv_idobs',y='waitingTime',ax=ha[1,1],data=df_data_subj.loc[df_data_subj.greedy_idobs,:],
                color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    sns.regplot(x='dv_idobs',y='waitingTime',ax=ha[1,1],data=df_data_subj.loc[np.logical_not(df_data_subj.greedy_idobs),:],
                color='xkcd:brick', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)

    plt.tight_layout()
    plt.suptitle(subj)
    hf.savefig("figures/WTs_idobs_vs_sarsa_" + subj + ".pdf")

#sns.regplot(x='dv_sarsa',y='isChoiceLeft',ax=ha[0],data=df_data.loc[df_data.greedy_sarsa,:],
#            color='xkcd:leaf green', y_jitter=y_jitter,x_bins=nbins,fit_reg=fit_reg,ci=None)
#sns.regplot(x='dv_sarsa',y='isChoiceLeft',ax=ha[0],data=df_data.loc[np.logical_not(df_data.greedy_sarsa),:],color='xkcd:brick', y_jitter=y_jitter,x_bins=100)

# ha.plot(Ql_sarsa)
# ha.plot(Qr_sarsa)
#
# hf.savefig("notebooks/Q_sarsa.png")
#
# #%%
#
# stan_data['Ql_sarsa']=Ql_sarsa
# stan_data['Qr_sarsa']=Qr_sarsa