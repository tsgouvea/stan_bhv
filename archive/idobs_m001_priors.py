#!/usr/bin/env python
# coding: utf-8
# Model 001: hierarchical eta, no beta

import os
import pickle

import numpy as np
import seaborn as sns

from locallib import StanModel_cache

# decision completely controlled by learn_pi estimate of reward probability
# ## stan_code_idobs
# ### without Beta, hierarchical Eta

stan_code_m001_priors = """data {
  int<lower=0> N;
  int<lower=1> L;
  int<lower=1> S;
  int<lower=0,upper=1> isChoiceLeft[N];
  int<lower=0,upper=1> isRewarded[N];
  int<lower=1,upper=L> subj[N];
  int<lower=1,upper=S> sess[N];
  int<lower=1,upper=L> subj_per_sess[S];
}
parameters {
  real<lower=0> m1;
  real<lower=0> s1;
  real<lower=0> m2;
  real<lower=0> s2;
  real<lower=0> m3;
  real<lower=0> s3;
  real<lower=0> p1;
  real<lower=0> p2;
  real<lower=0> p3;
  real<lower=0> beta;
  real<lower=0,upper=1> eta;
}
model {
  m1 ~ cauchy(0,100);
  s1 ~ cauchy(0,100);
  m2 ~ cauchy(0,100);
  s2 ~ cauchy(0,100);
  m3 ~ cauchy(0,100);
  s3 ~ cauchy(0,100);
  p1 ~ gamma(m1,s1);
  p2 ~ gamma(m2,s2);
  p3 ~ gamma(m3,s3);
  beta ~ cauchy(0,p1);
  eta ~ beta(p2,p3);
}
"""
sm_m001_priors = StanModel_cache(model_code=stan_code_m001_priors)

with open(os.path.join('data', 'stan_data.pickle'), 'rb') as fhandle:
    stan_data = pickle.load(fhandle)

fname_fit_m001_priors = os.path.join('data','fit_m001_priors.pickle')
if os.path.isfile(fname_fit_m001_priors):
    print('loading fit object')
    with open(fname_fit_m001_priors,'rb') as fhandle:
        d = pickle.load(fhandle)
        fit_m001_priors = d['fit']
else:
    fit_m001_priors = sm_m001_priors.sampling(data=stan_data, iter=1000, chains=4, verbose=True)
    pickle.dump({'model': sm_m001_priors, 'fit': fit_m001_priors}, open(fname_fit_m001_priors, 'wb'), protocol=-1)

#print(fit_m001_priors)

myvars = np.array(fit_m001_priors.flatnames)#[[not n.startswith('log_like') and not n.startswith('Q') for n in fit_m001_priors.flatnames]]
df_diag_m001_priors = fit_m001_priors.to_dataframe()

#hf = sns.pairplot(df_diag_m001_priors, hue='chain', vars=myvars,kind='kde', diag_kind='kde')
hf = sns.pairplot(df_diag_m001_priors, hue='chain', vars=['p1','p2','p3','beta','eta'],kind='kde', diag_kind='kde')

hf.savefig("notebooks/pairplot_idobs_m001_priors.png")