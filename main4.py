import os
import pickle

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt

from locallib import fit, optim

for imodel in [107]:#range(2,11):
    model_name = 'm{:03d}'.format(imodel)
    print(model_name)
    for dataset in ['matching_fix','matching_conf']:
        print(dataset)
        try:
            fit(dataset,model_name,warmup=200,iter=400)
        except Exception as e:
            print(e)
        try:
            optim(dataset,model_name,init='fit')
        except Exception as e:
            print(e)